#include <algorithm>
#include <cstring>
#include <iostream>
#include <list>
#include <numeric>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

template <typename T>
T my_max(T a, T b)
{
    return a < b ? b : a;
}

const char* my_max(const char* a, const char* b)
{
    return strcmp(a, b) < 0 ? b : a;
}

struct Gadget
{
    string name;

    bool operator<(const Gadget& other) const
    {
        return name < other.name;
    }
};

TEST_CASE("test")
{
    REQUIRE(my_max(4, 5) == 5);
    REQUIRE(my_max(4.15, 5.15) == Approx(5.15));

    vector<int> vec1 = {1, 2, 3};
    vector<int> vec2 = {4, 5, 6};
    vector<int> vec_max = my_max(vec1, vec2);

    REQUIRE(vec_max == vector<int>{4, 5, 6});

    Gadget g_max = my_max(Gadget{"ipad"}, Gadget{"ipod"});
    REQUIRE(g_max.name == "ipod"s);

    REQUIRE(my_max(static_cast<double>(1), 3.14) == 3.14);
    REQUIRE(my_max<double>(1, 3.14) == 3.14);

    const char* txt1 = "ola";
    const char* txt2 = "ala";

    const char* txt_max = my_max(txt1, txt2);
    REQUIRE(strcmp(txt_max, "ola") == 0);
}

template <typename T>
bool is_greater(T a, T b)
{
    return a > b;
}

TEST_CASE("address of template function")
{
    bool (*ptr_fun)(int, int) = &is_greater;

    auto ptr_fun_2 = &is_greater<int>;

    REQUIRE(ptr_fun == ptr_fun_2);
}

template <typename TResult, typename T1, typename T2>
TResult add1(T1 a, T2 b)
{
    return a + b;
}

template <typename T1, typename T2>
auto add2(T1 a, T2 b)
{
    return a + b;
}

template <typename T1, typename T2>
std::common_type_t<T1, T2> add3(T1 a, T2 b)
{
    return a + b;
}

TEST_CASE("return types")
{
    REQUIRE(add1<double>(1, 3.14) == Approx(4.14));

    REQUIRE(add2("abc"s, "def") == "abcdef"s);

    REQUIRE(add3(1, 3.14) == Approx(4.14));
}

bool is_even(int n)
{
    return n % 2 == 0;
}

template <typename Iterator, typename Predicate>
constexpr Iterator my_find_if(Iterator first, Iterator last, Predicate predicate)
{
    for (Iterator it = first; it != last; ++it)
        if (predicate(*it))
            return it;
    return last;
}

struct GreaterThan // functor === function object
{
    int value;

    bool operator()(int n) const
    {
        return n > value;
    }
};

TEST_CASE("GreaterThan")
{
    GreaterThan gt{4};

    REQUIRE(gt(5));
}

TEST_CASE("find_if")
{
    vector<int> vec = {1, 5, 7, 9, 665, 42, 667, 99};

    auto pos_even = my_find_if(vec.begin(), vec.end(), &is_even);

    REQUIRE(*pos_even == 42);

    auto gt_100 = my_find_if(vec.begin(), vec.end(), GreaterThan{100});

    REQUIRE(*gt_100 == 665);

    vector<string> words = {"one", "two", "three", "", "four"};

    auto pos_empty = find_if(words.begin(), words.end(), [](const string& word) { return word == ""s; });

    REQUIRE(*pos_empty == ""s);

    int numbers[] = {1, 2, 3, 4, 5, -1, 0, 45};

    auto pos_negative = find_if(begin(numbers), end(numbers), [](int x) { return x < 0; });

    REQUIRE(*pos_negative == -1);

    constexpr std::array<int, 8> arr = {1, 2, 3, 5, 6, 7, 8};
    constexpr auto gt_5 = *my_find_if(arr.begin(), arr.end(), [](int x) { return x > 5; });

    static_assert(gt_5 == 6, "Error");
}

TEST_CASE("algorithms")
{
    vector<int> vec = {1, 5, 7, 9, 665, 42, 667, 88, 6, 99};

    SECTION("remove_if")
    {
        auto is_odd = [](int n) { return n % 2 != 0; };

        auto new_end = remove_if(vec.begin(), vec.end(), is_odd);
        vec.erase(new_end, vec.end());

        for (const auto& n : vec)
            cout << n << " ";
        cout << "\n";
    }

    SECTION("partition")
    {
        auto threshold = partition(vec.begin(), vec.end(), &is_even);

        cout << "evens: ";
        for (auto it = vec.begin(); it != threshold; ++it)
            cout << *it << " ";

        cout << "\nodds: ";
        for (auto it = threshold; it != vec.end(); ++it)
            cout << *it << " ";
        cout << "\n";
    }

    SECTION("sorting")
    {
        sort(vec.begin(), vec.end());

        for (const auto& n : vec)
            cout << n << " ";
        cout << "\n";

        sort(vec.begin(), vec.end(), greater<int>{});

        for (const auto& n : vec)
            cout << n << " ";
        cout << "\n";
    }
}

struct Math
{
    static constexpr double pi = 3.14;
};

void foo()
{
}

template <typename T>
void deduce1(T arg)
{
    puts(__PRETTY_FUNCTION__);
}

TEST_CASE("auto type deduction - case 1")
{
    deduce1(5);
    auto x = 5;

    string s = "abc";
    deduce1(s);
    auto as1 = s; // string

    string& ref_s = s;
    deduce1(ref_s);
    auto as2 = ref_s; // string

    const string& cref_s = s;
    deduce1(cref_s);
    auto as3 = cref_s; // string

    int tab[10];
    deduce1(tab);
    auto atab = tab; // int*

    deduce1(foo);
    auto af = foo;
}

template <typename T>
void deduce2(T& arg)
{
    puts(__PRETTY_FUNCTION__);
}

TEST_CASE("auto type deduction - case 2")
{
    int x = 10;
    const int cx = 10;
    int& ref_x = x;
    const int& cref_x = cx;
    int tab[10];

    deduce2(x);
    auto& ax1 = x; // int&

    deduce2(cx);
    auto& ax2 = cx; // const int&

    deduce2(ref_x);
    auto& ax3 = ref_x; // int&

    deduce2(cref_x);
    auto& ax4 = cref_x; // const int&

    deduce2(tab);
    auto& atab = tab; // int(&)[10]

    deduce2(foo);
    auto& ref_fun = foo;
}

TEST_CASE("auto - special case")
{
    auto lst = {1, 2, 3}; // std::initializer_list
}

TEST_CASE("using auto")
{
    vector<long long> vec = {1, 2, 3, 4, 5};

    for (auto& item : vec)
    {
        item = item * 2;
    }

    for (const auto& item : vec)
    {
        cout << item << " ";
    }
    cout << "\n";
}

template <typename T1, typename T2>
struct Pair
{
    T1 first;
    T2 second;

    Pair(const T1& f, const T2& s)
        : first{f}
        , second{s}
    {
    }

    void print() const
    {
        cout << "Pair(" << first << ", " << second << ")\n";
    }
};

// deduction guide - C++17
template <typename T1, typename T2>
Pair(T1, T2)->Pair<T1, T2>;

TEST_CASE("using pair")
{
    Pair<int, double> p1{1, 3.14};
    p1.print();

    Pair<string, string> p2{"text", "abc"};
    p2.print();

    SECTION("since C++17")
    {
        Pair p3{1, "text"};
        p3.print();

        Pair p4{1, 3.14};
        p4.print();
    }

    vector<int> vec = {1, 2, 4};

    Pair pit(vec.cbegin(), vec.cend());
}
