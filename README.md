# Advanced C++ Programming

## Doc

* https://infotraining.bitbucket.io/cpp-adv
* https://infotraining.bitbucket.io/cpp-11/lambdas.html


## Virtual Machine

### login and password for VM:

```
dev  /  pwd
```

### reinstall VBox addon (optional)

```
sudo /etc/init.d/vboxadd setup
```

### proxy settings (optional)

We can add them to `.profile`

```
export http_proxy=http://aaa.bbb.ccc.ddd:port
export https_proxy=https://aaa.bbb.ccc.ddd:port
```

### vcpkg settings

Add to `.profile`

```
export VCPKG_ROOT="/usr/share/vcpkg" 
export CC="/usr/bin/gcc-9"
export CXX="/usr/bin/g++-9"
```

## GIT

```
git clone https://bitbucket.org/infotraining/cpp-adv-2019-10-28.git
```

## Links

* [git cheat sheet](http://www.cheat-sheets.org/saved-copy/git-cheat-sheet.pdf)
* [C++ Core Guidelines](http://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines)
* [C++ Reference](https://en.cppreference.com)
* [vcpkg](https://github.com/microsoft/vcpkg)

## Ankieta

* https://www.infotraining.pl/ankieta/cpp-adv-2019-10-28-kp
