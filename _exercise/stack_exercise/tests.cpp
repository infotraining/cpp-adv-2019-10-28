#include "catch.hpp"
#include <algorithm>
#include <array>
#include <deque>
#include <memory>

template <typename T, typename Container = std::deque<T>>
class Stack
{
    Container items_;

public:
    Stack() = default;

    bool empty() const
    {
        return items_.empty();
    }

    size_t size() const;

    void push(const T& item)
    {
        items_.push_back(item);
    }

    void push(T&& item)
    {
        items_.push_back(std::move(item));
    }

    template <typename... Args>
    void emplace(Args&&... args)
    {
        items_.emplace_back(std::forward<Args>(args)...);
    }

    T& top()
    {
        return items_.back();
    }

    const T& top() const
    {
        return items_.back();
    }

    void pop()
    {
        items_.pop_back();
    }
};

template <typename T, typename Container>
size_t Stack<T, Container>::size() const
{
    return items_.size();
}

struct Gadget
{
    int id;
    std::string name;

    Gadget(int id)
        : id{id}
        , name{"unkown"}
    {
    }

    Gadget(int id, std::string name)
        : id{id}
        , name{std::move(name)}
    {
    }
};

TEST_CASE("emplace")
{
    Stack<Gadget> s;

    s.push(Gadget{1, "ipad"});
    s.emplace(42, "ipod");
    s.emplace(667);
}

TEST_CASE("After construction", "[stack,constructors]")
{
    Stack<int, std::vector<int>> s;

    SECTION("is empty")
    {
        REQUIRE(s.empty());
    }

    SECTION("size is zero")
    {
        REQUIRE(s.size() == 0);
    }
}

TEST_CASE("Pushing an item", "[stack,push]")
{
    Stack<int> s;

    SECTION("is no longer empty")
    {
        s.push(1);

        REQUIRE(!s.empty());
    }

    SECTION("size is increased")
    {
        auto size_before = s.size();

        s.push(1);

        REQUIRE(s.size() - size_before == 1);
    }

    SECTION("recently pushed item is on a top")
    {
        s.push(4);

        REQUIRE(s.top() == 4);
    }
}

template <typename T>
std::vector<T> pop_all(Stack<T>& s)
{
    std::vector<T> values(s.size());

    for (auto& item : values)
    {
        item = std::move(s.top());
        s.pop();
    }

    return values;
}

TEST_CASE("Popping an item", "[stack,pop]")
{
    Stack<int> s;

    s.push(1);
    s.push(4);

    int item;

    SECTION("assignes an item from a top to an argument passed by ref")
    {
        item = s.top();
        s.pop();

        REQUIRE(item == 4);
    }

    SECTION("size is decreased")
    {
        size_t size_before = s.size();

        item = s.top();
        s.pop();

        REQUIRE(size_before - s.size() == 1);
    }

    SECTION("LIFO order")
    {
        int a, b;

        a = s.top();
        s.pop();

        b = s.top();
        s.pop();

        REQUIRE(a == 4);
        REQUIRE(b == 1);
    }
}

TEST_CASE("Move semantics", "[stack,push,pop,move]")
{
    using namespace std::literals;

    SECTION("stores move-only objects")
    {
        auto txt1 = std::make_unique<std::string>("test1");

        Stack<std::unique_ptr<std::string>> s;

        s.push(move(txt1));
        s.push(std::make_unique<std::string>("test2"));

        std::unique_ptr<std::string> value;

        value = std::move(s.top());
        s.pop();
        REQUIRE(*value == "test2"s);

        value = std::move(s.top());
        s.pop();
        REQUIRE(*value == "test1"s);
    }

    SECTION("move constructor", "[stack,move]")
    {
        Stack<std::unique_ptr<std::string>> s;

        s.push(std::make_unique<std::string>("txt1"));
        s.push(std::make_unique<std::string>("txt2"));
        s.push(std::make_unique<std::string>("txt3"));

        auto moved_s = std::move(s);

        auto values = pop_all(moved_s);

        auto expected = {"txt3", "txt2", "txt1"};
        REQUIRE(std::equal(values.begin(), values.end(), expected.begin(), [](const auto& a, const auto& b) { return *a == b; }));
    }

    SECTION("move assignment", "[stack,move]")
    {
        Stack<std::unique_ptr<std::string>> s;

        s.push(std::make_unique<std::string>("txt1"));
        s.push(std::make_unique<std::string>("txt2"));
        s.push(std::make_unique<std::string>("txt3"));

        Stack<std::unique_ptr<std::string>> target;
        target.push(std::make_unique<std::string>("x"));

        target = std::move(s);

        REQUIRE(target.size() == 3);

        auto values = pop_all(target);

        auto expected = {"txt3", "txt2", "txt1"};
        REQUIRE(std::equal(values.begin(), values.end(), expected.begin(), [](const auto& a, const auto& b) { return *a == b; }));
    }
}

TEST_CASE("std::array")
{
    int tab[100] = {1, 2, 3, 5};

    SECTION("since c++11")
    {
        std::array<int, 100> arr = {1, 2, 3, 5};

        std::sort(arr.begin(), arr.end(), std::greater<int>{});

        REQUIRE(arr.size() == 100);

        REQUIRE(arr[0] == 5);

        arr.fill(0);
    }
}
