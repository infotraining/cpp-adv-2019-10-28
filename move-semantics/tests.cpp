#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <chrono>

#include "catch.hpp"

using namespace std;

string full_name(const string& first, const string& last)
{
    return first + " " + last;
}

TEST_CASE("reference binding")
{
    string name = "Jan";

    SECTION("C++98")
    {
        SECTION("l-value can be bound to l-value ref")
        {
            string& ref_name = name;
            REQUIRE(ref_name.size() == name.size());
        }

        SECTION("r-value can be bound to const l-value ref")
        {
            const string& ref_full_name = full_name(name, "Kowalski");
        }
    }

    SECTION("C++11")
    {
        SECTION("r-value can be bound to r-value ref")
        {
            string&& ref_full_name = full_name(name, "Kowalski");

            ref_full_name[0] = 'P';
            REQUIRE(ref_full_name == "Pan Kowalski"s);
        }

        SECTION("l-value can not be bound to r-value ref")
        {
            //string&& ref_name = name;  // COMPILER ERROR
        }
    }
}

TEST_CASE("moving objects")
{
    string str = "Ala";

    string other = str; // implicit copy of l-value

    string target = std::move(str); // explicit move of l-value (after move l-value becomes x-value)

    cout << target << "\n";

    SECTION("illegal but safe in std library")
    {
        cout << str.size() << " - " << str << "\n";
    }

    string fn = full_name("Jan", "Kowalski");

    vector<string> names;

    names.push_back(full_name("Ewa", "Nowak"));
    names.push_back(std::move(fn));
}

class MyVector
{
    int* array_;
    size_t size_;

public:
    using iterator = int*;
    using const_iterator = const int*;

    iterator begin()
    {
        return array_;
    }

    iterator end()
    {
        return array_ + size_;
    }

    const_iterator begin() const
    {
        return array_;
    }

    const_iterator end() const
    {
        return array_ + size_;
    }

    void print_items(const std::string& msg) const noexcept
    {
        std::cout << msg << " : { ";

        if (array_)
        {
            for (int* it = array_; it != array_ + size_; ++it)
                std::cout << *it << " ";
        }
        else
        {
            std::cout << "AFTER MOVE ";
        }
        std::cout << "}\n";
    }

    MyVector(std::initializer_list<int> lst)
        : array_(new int[lst.size()])
        , size_(lst.size())
    {
        std::copy(lst.begin(), lst.end(), array_);
        print_items("MyVector ctor");
    }

    MyVector(const MyVector& source)
        : array_(new int[source.size_])
        , size_{source.size_}
    {
        std::copy(source.array_, source.array_ + size_, array_);
        print_items("MyVector cpy ctor");
    }

    MyVector& operator=(const MyVector& source)
    {
        if (this != &source)
        {
            delete[] array_;
            array_ = new int[source.size_];
            size_ = source.size_;

            std::copy(source.array_, source.array_ + size_, array_);
        }

        print_items("MyVector cpy =");
        return *this;
    }

    MyVector(MyVector&& source) noexcept(noexcept(source.print_items(std::declval<string>()))) // move constructor
        : array_{source.array_}
        , size_{source.size_}
    {
        source.array_ = nullptr;

        print_items("MyVector move ctor");
    }

    MyVector& operator=(MyVector&& source) noexcept(noexcept(source.print_items(std::declval<string>())))
    {
        if (this != &source)
        {
            delete[] array_;

            array_ = source.array_;
            size_ = source.size_;

            source.array_ = nullptr;

            print_items("MyVector move =");
        }

        return *this;
    }

    ~MyVector()
    {
        print_items("MyVector dtor");
        delete[] array_;
    }

    size_t size() const
    {
        return size_;
    }

    int& operator[](size_t index)
    {
        return array_[index];
    }

    const int& operator[](size_t index) const
    {
        return array_[index];
    }
};

MyVector create_data()
{
    MyVector vec = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    return vec;
}

class CopyableOnly
{
    const MyVector data; // const member can be only copied

public:
    CopyableOnly(MyVector vec)
        : data(std::move(vec))
    {
    }
};

TEST_CASE("MyVector")
{
    std::vector<int> v = {1, 2, 3, 4, 5, 6};

    MyVector vec = {1, 2, 3, 4, 5, 6};
    REQUIRE(vec.size() == 6);
    REQUIRE(vec[0] == 1);
    REQUIRE(vec[5] == 6);

    //MyVector other = vec; // copy constructor

    MyVector vec2 = {1, 2, 3};
    //vec2 = vec; // copy assignment

    MyVector data = create_data();
    MyVector target = std::move(data);
    vec2 = std::move(target);
    vec2.print_items("vec2");

    {
        cout << "\n^^^^^^^^^^^^^^^^^^^^^^\n";
        CopyableOnly co(create_data());
        CopyableOnly other = std::move(co);
    }
}

struct Dataset
{
    std::string name;
    std::vector<MyVector> data;
    MyVector row;

public:
    Dataset(std::string id, MyVector r)
        : name(std::move(id))
        , row(std::move(r))
    {
    }

    Dataset(const Dataset&) = default;
    Dataset& operator=(const Dataset&) = default;
    Dataset(Dataset&&) = default;
    Dataset& operator=(Dataset&&) = default;

    ~Dataset()
    {
        std::cout << "~Dataset: " << name << "\n";
    }

    void add(const MyVector& vec)
    {
        data.push_back(vec);
    }

    void add(MyVector&& vec)
    {
        data.push_back(std::move(vec));
    }

    void print() const
    {
        std::cout << "Dataset: " << name << "\n";
        for (const auto& vec : data)
        {
            std::cout << " + ";
            for (const auto& item : vec)
                std::cout << item << " ";
            std::cout << "\n";
        }
        row.print_items(" $");
        std::cout << "\n";
    }
};

Dataset load_from_file()
{
    Dataset ds("data", create_data());
    ds.add(create_data());
    ds.add(create_data());

    return ds;
}

TEST_CASE("Dataset")
{
    cout << "\n####################\n";
    Dataset data_set = load_from_file();

    data_set.print();

    Dataset target = std::move(data_set);
    target.print();
}

using ReturnType = decltype(full_name(std::declval<string>(), std::declval<string>()));

TEST_CASE("noexcept")
{
    cout << "\n************************\n";

    std::vector<MyVector> vec;

    vec.push_back(MyVector{1, 2, 3, 4});
    vec.push_back(create_data());
    vec.push_back(MyVector{7, 56, 2345, 665});
    vec.push_back(MyVector{665, 667});
    vec.push_back(MyVector{9, 8, 7});

    static_assert(is_same<ReturnType, string>::value, "Error");
}

using Matrix = vector<vector<int>>;

Matrix create_large_matrix(size_t n = 1'000'000)
{
    Matrix matrix(n);

    for (auto& row : matrix)
        row = std::vector<int>(n);

    return matrix;
}

TEST_CASE("using matrix")
{
    Matrix m = create_large_matrix(1'000);

    m[665][665] = 13;

    REQUIRE(m[665][665] == 13);
}

class Moveable
{
    int id_;
    string txt_;
public:
    Moveable(int id, string txt)
        : id(id), txt_(std::move(txt))
    {

    }

    void print() const
    {
        cout << "Moveable(" << id_ << ", " << txt_ << ")" << endl;
    }
};

TEST_CASE("recap")
{
    int x;
    x = 6;
    const char* txt = "txt";
    string str = "txt"s; // r-value

    auto timespan = 100ms;
    auto ts = chrono::milliseconds(500);

    Moveable mv1(1, "text");
    string abc = "abc";
    Moveable mv2(2, abc);

    Moveable backup = mv1;
    Moveable target = std::move(mv1);
}