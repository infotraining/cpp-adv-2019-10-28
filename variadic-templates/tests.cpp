#include <algorithm>
#include <boost/functional/hash.hpp>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

namespace Cpp11
{

    void print()
    {
        cout << "\n";
    }

    template <typename Head, typename... Tail>
    void print(const Head& head, const Tail&... tail)
    {

        cout << sizeof...(tail) << ": " << head << "; ";
        print(tail...);
    }
}

template <typename... Args>
void print(const Args&... args)
{
    bool first = true;

    auto with_space = [&first](const auto& arg) {
        if (first)
            first = false;
        else
            cout << " ";

        return arg;
    };

    (cout << ... << with_space(args)) << "\n";
}

TEST_CASE("variadic templates")
{
    print(1, 3.14, "text"s, "abc");

    print(44.44f);

    print();
}

namespace Cpp14
{
    template <typename Head>
    Head sum(Head h)
    {
        return h;
    }

    template <typename Head, typename... Tail>
    auto sum(Head h, Tail... tail)
    {
        return h + sum(tail...);
    }
}

template <typename... Args>
constexpr auto sum(Args... args)
{
    return (... + args);
}

TEST_CASE("sum")
{
    auto result = sum(1, 2, 3, 4, 5);

    REQUIRE(result == 15);

    static_assert(sum(1, 2, 3, 4, 5) == 15, "Error");

    int tab[sum(1, 2, 3, 4, 5)];

    static_assert(sizeof(tab) / sizeof(int) == 15);
}

template <typename T>
void better_hash_combine(size_t& seed, const T& value)
{
    seed ^= hash<T>{}(value) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}

template <typename... Args>
size_t combined_hash(const Args&... args)
{
    size_t seed = 0;
    (..., better_hash_combine(seed, args));
    return seed;
}

TEST_CASE("hash_combine")
{
    constexpr int x = 10;
    constexpr short f = 423;
    constexpr char pi = 'p';

    size_t seed = 0;
    boost::hash_combine(seed, x);
    boost::hash_combine(seed, f);
    boost::hash_combine(seed, pi);

    REQUIRE(seed == 543221708272712659);

    REQUIRE(combined_hash(x, f, pi) == 11093822785774);
}

constexpr uintmax_t factorial(uintmax_t n)
{
    if (n == 0)
        return 1;

    return n * factorial(n - 1);
}

TEST_CASE("factorial")
{
    static_assert(factorial(1) == 1, "Error");
    static_assert(factorial(2) == 2, "Error");
    static_assert(factorial(3) == 6, "Error");
}

constexpr std::array<uintmax_t, 10> create_lookup_table()
{
    std::array<uintmax_t, 10> factorials{};

    for (size_t i = 0; i < factorials.size(); ++i)
        factorials[i] = factorial(i);

    return factorials;
}

TEST_CASE("lookup table")
{
    constexpr auto factorials = create_lookup_table();

    static_assert(factorials[5] == 120, "Error");

    auto square = [](int x) { return x * x; };

    int tab[square(8)];
}
