#include <algorithm>
#include <iostream>
#include <numeric>
#include <queue>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

void foo(int x)
{
    cout << "foo(" << x << ")\n";
}

class Foo
{
    int counter_ = 0;

public:
    void operator()(int x)
    {
        cout << "Foo::operator(" << x << ")\n";
        ++counter_;
    }

    int count() const
    {
        return counter_;
    }
};

TEST_CASE("callable types")
{
    SECTION("function pointer")
    {
        using FooPtr = void (*)(int);

        FooPtr f = foo;

        f(42); // call
    }

    SECTION("function objects - functors")
    {
        Foo f;

        f(665); // call
        f(42);

        REQUIRE(f.count() == 2);
    }

    SECTION("lambdas")
    {
        auto f = [](int x) { cout << "lambda_f(" << x << ")\n"; };

        f(42);
        f(665);
    }
}

TEST_CASE("std::function")
{
    std::function<void(int)> f;

    f = foo;
    f(42);

    f = Foo{};
    f(665);

    f = [](int x) { cout << "lambda_f(" << x << ")\n"; };
    f(13);
}

class MagicLambda_42756w5rq86
{
public:
    bool operator()(int a, int b) const { return a > b; }
};

TEST_CASE("lambda expressions")
{
    auto gt = [](int a, int b) { return a > b; };

    REQUIRE(gt(2, 1));

    SECTION("is interpretes as")
    {
        MagicLambda_42756w5rq86 gt = MagicLambda_42756w5rq86{};
    }

    vector<int> vec = {6, 1, 354, 42, 1235, 346, 2345, 3458, 3456};
    sort(vec.begin(), vec.end(), gt);

    for_each(vec.begin(), vec.end(), [](int n) { cout << n << " "; });
    cout << "\n";

    SECTION("lambda without captured variables [] can be converted to fun ptr")
    {
        void (*callback)(const string& msg);

        callback = [](const string& msg) { cout << "LOG: " << msg << "\n"; };

        callback("Ok");
        callback("Error");
    }
}

class MagicLambda_4513420861379afjdg
{
    const int thshld_;

public:
    MagicLambda_4513420861379afjdg(int t)
        : thshld_{t}
    {
    }

    bool operator()(int x) const { return x > thshld_; }
};

auto get_printer()
{
    auto line = std::make_unique<std::string>(40, '*');

    return [line = std::move(line)]() { cout << (*line) << "\n"; };
}

TEST_CASE("capturing values")
{
    vector<int> vec = {42, 2453654, 34, 2346, 2354, 24, 234578, 7645};

    SECTION("by value")
    {
        int threshold = 100;
        auto discriminator = [threshold](int x) { return x > threshold; };

        threshold = 1'000'000;

        cout << "> 100: " << count_if(vec.begin(), vec.end(), discriminator) << "\n";
    }

    SECTION("by ref")
    {
        long long sum = 0;

        for_each(vec.begin(), vec.end(), [&sum](int x) { sum += x; });

        cout << "sum: " << sum << "\n";
    }

    SECTION("by value & ref")
    {
        long long sum = 0;
        int factor = 2;

        for_each(vec.begin(), vec.end(), [&sum, factor](int x) { sum += x * factor; });

        cout << "sum (by factor): " << sum << "\n";
    }

    SECTION("since C++14 - init expression")
    {
        {
            auto line = std::make_unique<std::string>(40, '*');

            auto printer = [line = std::move(line)]() { cout << (*line) << "\n"; };

            printer();
        }

        {
            auto prn = get_printer();

            prn();
            prn();
        }
    }
}

TEST_CASE("complex")
{
    int y = 1;
    int factor = 10;

    auto f = [=](int x) mutable {
        factor += 10;
        auto g = [=]() { cout << "catched: " << x * y << " " << factor << "\n"; };
        return g;
    };

    f(1)();
    f(2)();
}

auto create_generator(size_t seed)
{
    return [seed]() mutable { return ++seed; };
}

TEST_CASE("generator")
{
    auto gen = create_generator(100);

    REQUIRE(gen() == 101);
    REQUIRE(gen() == 102);
    REQUIRE(gen() == 103);

    vector<int> vec(100);

    generate(vec.begin(), vec.end(), create_generator(1000));

    for (const auto& item : vec)
        cout << item << " ";
    cout << "\n";
}

using Task = std::function<void()>;

class TaskQueue
{
    std::queue<Task> tasks_;

public:
    void submit(Task task)
    {
        tasks_.push(task);
    }

    void run()
    {
        while (!tasks_.empty())
        {
            auto atomic_pop = [this] {
                Task task;
                task = tasks_.front();
                tasks_.pop();

                return task;
            };

            Task task = atomic_pop();

            task();
        }
    }
};

void my_log(const string& msg)
{
    cout << "Log: " << msg << endl;
}

void stop()
{
    cout << "STOP" << endl;
}

TEST_CASE("task queue")
{
    TaskQueue q;

    q.submit([] { cout << "Start...\n"; });
    q.submit([] { cout << "One...\n"; });
    q.submit([] { my_log("Two"); });
    q.submit(&stop);

    cout << "\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n";

    q.run();
}

class MagicLambda_31254335476dfgjkdsghbcvtrut
{
    const double avg_;

public:
    MagicLambda_31254335476dfgjkdsghbcvtrut(double avg)
        : avg_{avg}
    {
    }

    template <typename T>
    auto operator()(T x) const { return x < avg_; }
};

TEST_CASE("generic lambdas")
{
    vector<double> vec = {1, 2.9, 3, 5, 53.9, 2453, 245, 3};

    double avg = accumulate(vec.begin(), vec.end(), 0.0) / vec.size();

    auto partition_pos = partition(vec.begin(), vec.end(), [avg](auto x) { return x < avg; });
}
