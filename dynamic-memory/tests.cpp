#include <algorithm>
#include <iostream>
#include <map>
#include <memory>
#include <numeric>
#include <string>
#include <thread>
#include <vector>

#include "catch.hpp"

class Gadget
{
    size_t id_;
    std::string name_;

public:
    Gadget(size_t id)
        : id_{id}
        , name_{"unknown"}
    {
    }

    Gadget(size_t id, std::string name = "not-set") // constructor
        : id_{id}, name_{std::move(name)}
    {
        std::cout << "Gadget(" << id_ << " - " << name_ << ": " << this << ")\n";
    }

    Gadget(const Gadget&) = delete;
    Gadget& operator=(const Gadget&) = delete;
    Gadget(Gadget&&) = delete;
    Gadget& operator=(Gadget&&) = delete;

    ~Gadget()
    {
        std::cout << "~Gadget(" << id_ << " - " << name_ << ": " << this << ")\n";
    }

    void use()
    {
        std::cout << "Using gadget " << id_ << " - " << name_ << ": " << this << "\n";
    }
};

namespace Legacy
{
    Gadget* create_gadget(const std::string& name = "Gadget")
    {
        static size_t counter = 0;
        size_t id = ++counter;
        return new Gadget(id, name + std::to_string(id));
    }

    void use1(Gadget* g)
    {
        if (g)
            g->use();
    }

    void use2(Gadget* g)
    {
        if (g)
            g->use();

        delete g;
    }
}

TEST_CASE("mem leak - dynamic memory hell")
{
    {
        Gadget* ptrg = Legacy::create_gadget("ipad");
        ptrg->use();

        Legacy::create_gadget("temp");
    } // mem leak
    
    { // after refactoring
        std::unique_ptr<Gadget> ptrg{Legacy::create_gadget("ipad")};
        ptrg->use();                
    } // mem leak

    {
        Legacy::use1(Legacy::create_gadget("ipad")); // mem leak
    }

    { // after refactoring
        std::unique_ptr<Gadget> g(Legacy::create_gadget("ipad"));
        Legacy::use1(g.get());
    }
    
    {
        Gadget g{665, "ipod"};
        Legacy::use1(&g);
        //Legacy::use2(&g); // SEGFAULT
    }
}

template <typename T>
class UniquePtr
{
    T* ptr_;

public:
    explicit UniquePtr(T* ptr)
        : ptr_{ptr}
    {
    }

    ~UniquePtr()
    {
        delete ptr_;
    }

    UniquePtr(const UniquePtr&) = delete;
    UniquePtr& operator=(const UniquePtr&) = delete;

    UniquePtr(UniquePtr&& source) noexcept
        : ptr_{source.ptr_}
    {
        source.ptr_ = nullptr;
    }

    UniquePtr& operator=(UniquePtr&& source) noexcept
    {
        if (this != &source)
        {
            delete ptr_;
            ptr_ = source.ptr_;
            source.ptr = nullptr;
        }

        return *this;
    }

    T* get() const
    {
        return ptr_;
    }

    T* release()
    {
        T* ptr = ptr_;
        ptr_ = nullptr;
        return ptr;
    }

    void reset(T* new_ptr = nullptr)
    {
        delete ptr_;
        ptr_ = new_ptr;
    }

    T& operator*()
    {
        return *ptr_;
    }

    T* operator->() const
    {
        return ptr_;
    }

    explicit operator bool() const
    {
        return ptr_ != nullptr;
    }
};

TEST_CASE("smart pointers - unique_ptr")
{
    std::cout << "\n%%%%%%%%%%%%%%%%%%%%%%%%\n";

    UniquePtr<Gadget> g1{new Gadget(655, "ipad")};

    (*g1).use();
    g1->use();

    UniquePtr<Gadget> g2 = std::move(g1);
    g2->use();

    if (g2)
        g2->use();
}

template <typename T, typename... Args>
std::unique_ptr<T> my_make_unique(Args&&... args)
{
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

//template <typename T, typename Arg1, typename Arg2>
//std::unique_ptr<T> my_make_unique(Arg1&& arg1, Arg2&& arg2)
//{
//    return std::unique_ptr<T>(new T(std::forward<Arg1>(arg1), std::forward<Arg2>(arg2)));
//}

namespace Modern
{
    std::unique_ptr<Gadget> create_gadget(const std::string& name = "Gadget")
    {
        static size_t counter = 0;
        size_t id = ++counter;

        //return std::unique_ptr<Gadget>{new Gadget(id, name + std::to_string(id))};
        //return my_make_unique<Gadget>(id, name + std::to_string(id));
        //return my_make_unique<Gadget>(id);

        return std::make_unique<Gadget>(id, name + std::to_string(id));
    }

    void use1(Gadget* g)
    {
        if (g)
            g->use();
    }

    void use2(std::unique_ptr<Gadget> g)
    {
        if (g)
            g->use();
    }
}

TEST_CASE("leak - free")
{
    {
        std::unique_ptr<Gadget> ptrg = Modern::create_gadget("ipad");
        ptrg->use();

        Modern::create_gadget("temp");

        Modern::use2(std::move(ptrg));
    } // leak free

    {
        std::unique_ptr<Gadget> g = Modern::create_gadget("ipad");
        Modern::use1(g.get());

        Legacy::use2(g.release());
    }

    {
        Gadget g{665, "ipod"};
        Modern::use1(&g);
    }
}

class Person
{
    std::string name_;
    std::unique_ptr<Gadget> gadget_;

public:
    Person(std::string name, std::unique_ptr<Gadget> g)
        : name_{std::move(name)}
        , gadget_{std::move(g)}
    {
    }

    void play() const
    {
        std::cout << name_ << " is ";
        gadget_->use();
    }
};

TEST_CASE("person & gadget")
{
    Person p1{"Janek", Modern::create_gadget()};
    p1.play();

    Person p2 = std::move(p1);
    p2.play();

    std::cout << "\n&&&&&&&&&&&&&&&&&&&&\n";

    {
        std::vector<std::unique_ptr<Gadget>> gadgets;

        gadgets.push_back(Modern::create_gadget());
        auto g = Modern::create_gadget();
        gadgets.push_back(std::move(g));

        for (const auto& ptr_g : gadgets)
            ptr_g->use();
    }
}

TEST_CASE("shared pointers")
{
    std::map<std::string, std::shared_ptr<Gadget>> gadget_pool;

    {
        std::shared_ptr<Gadget> g1 = std::make_shared<Gadget>(665, "ipad"); // rc == 1
        g1->use();

        {
            std::shared_ptr<Gadget> g3 = std::make_shared<Gadget>(1024, "mp3");
            g3->use();

            g3 = g1;

            std::shared_ptr<Gadget> g2 = g1; // rc == 2

            gadget_pool.insert(std::make_pair("ipad", g1)); // rc == 3

            REQUIRE(g1.use_count() == 4);
        } // rc == 2
    } // rc == 1

    gadget_pool["ipad"]->use();

    gadget_pool.clear(); // rc == 0 -> delete Gadget(665, "ipad")
}
