#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <tuple>
#include <vector>

#include "catch.hpp"

using namespace std;

tuple<int, int, double> calc_stats(const vector<int>& data)
{
    vector<int>::const_iterator min_pos, max_pos;
    tie(min_pos, max_pos) = minmax_element(data.begin(), data.end());

    double avg = accumulate(data.begin(), data.end(), 0.0) / data.size();

    return make_tuple(*min_pos, *max_pos, avg);
}

namespace Cpp17
{
    tuple<int, int, double> calc_stats(const vector<int>& data)
    {
        auto [min_pos, max_pos] = minmax_element(data.begin(), data.end()); // structured binding

        double avg = accumulate(data.begin(), data.end(), 0.0) / data.size();

        return tuple(*min_pos, *max_pos, avg);
    }
}

TEST_CASE("tuples")
{
    vector<int> data = {1, 345, 2345, 13, 52, 665, 543};

    auto stats = calc_stats(data);

    REQUIRE(get<0>(stats) == 1);
    REQUIRE(get<1>(stats) == 2345);
    REQUIRE(get<2>(stats) == Approx(566.285714));

    int min, max;
    double avg;

    tie(min, max, avg) = calc_stats(data);

    SECTION("ignore")
    {
        int min, max;

        tie(min, max, std::ignore) = calc_stats(data);
    }

    SECTION("C++17")
    {
        auto [min, max, avg] = calc_stats(data);
    }

    REQUIRE(min == 1);
    REQUIRE(max == 2345);
    REQUIRE(avg == Approx(566.285714));
}

TEST_CASE("tuple with refs")
{
    int x, y;
    double z;

    tuple<int&, int&, double&> tpl(x, y, z);

    get<0>(tpl) = 13;

    REQUIRE(x == 13);

    //tpl = make_tuple(42, 665, 3.14);
    tie(x, y, z) = make_tuple(42, 665, 3.14);

    REQUIRE(x == 42);
    REQUIRE(y == 665);
    REQUIRE(z == Approx(3.14));
}

struct Person
{
    string fname, lname;
    int age;

    auto tied() const
    {
        return tie(lname, fname, age);
    }

    bool operator==(const Person& p) const
    {
        return tied() == p.tied();
    }

    bool operator<(const Person& p) const
    {
        return tied() < p.tied();
    }
};

TEST_CASE("operators")
{
    vector<Person> people = {Person{"jan", "kowalski", 42}, Person{"adam", "nowak", 33}, Person{"ewa", "nowak", 23}};

    sort(people.begin(), people.end(), [](const Person& p1, const Person& p2) { return p1.tied() > p2.tied(); });
    //reverse(people.begin(), people.end());

    for (const auto& p : people)
        cout << p.fname << " " << p.lname << " " << p.age << "\n";
}
